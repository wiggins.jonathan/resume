# Jonathan Wiggins
## Senior Software Engineer | DevOps | Site Reliability

[jonathan@wiggins.tech](mailto:jonathan@wiggins.tech)<br>
[https://github.com/wiggins-jonathan](https://github.com/wiggins-jonathan)<br>

480-264-0077<br>

---

### Skills & Languages
- **Virtualization & Containerization**: AWS, Docker, Kubernetes, VirtualBox, VMware, Proxmox
- **CI/CD**: Terraform, Packer, Ansible, Gitlab CI/CD, Puppet, Jenkins
- **Coding Languages**: Go, Javascript, Python, Java, Bash, Lua, Perl, Nix
- **Frameworks**: React, Svelte, JQuery, Flask, Django, Spring Boot, Maven, Gradle
- **Data Analysis & Visualization**: Numpy, Pandas, Matplotlib, Grafana/Prometheus, Google Analytics
- **Human Languages**: English, Spanish

---

### Experience
#### State Farm | _Lead Software Engineer_ | 2018 - Present

- Leads the State Farm IaaS Cloud Solutions team in charge of building & maintaining custom solutions which automate & orchestrate complex tasks & infrastructure for internal business partners.
- Use multiple programming languages, frameworks, & APIs to automate the creation, destruction, maintenance & upgrade of cloud-based servers, containers, virtualised systems, & programs.
- The team routinely saves the company more than $30MM every year through automation & cloud infrastructure creation.
- Developed an application that backs up entire legacy Pivotal Cloud Foundry foundations for upgrade & data continuity purposes.
- Lead development of a proof-of-concept program that moves Java Spring Boot apps from on-prem to AWS EC2 or ECS Fargate with no source code changes. This codebase was then adopted by the entire company as an upgrade path for legacy applications to get to the cloud.
- Lead the modernization of a monolithic self-hosted web app from jquery & Perl to microservices in the cloud using AWS, Terraform, React & Go.
- Part of the Campus Recruit Team that recruits, interviews, & onboards interns from all three major AZ universities.

#### Ascend Learning | _DevOps Engineer II_ | 2014 - 2017

- Helped design & maintain a PaaS online insurance & securities school using Javascript, Python, PostgreSQL, Docker, AWS, BrainTree & FedEx APIs that produced $20MM in revenue for 2016.
- Produced custom interactive web dashboards for management using HTML/CSS, Flask, PostgreSQL, & Docker.
- Tracked & analyzed call flows, abandon rates, & service levels in a call center environment.
- Created & maintained a kanban-based inventory & shipping system for the Shipping team.
- Implemented eventing, logging, & visualization using Grafana/Prometheus to determine service-level indicators (SLIs).
- Won 2 “Employee of the Month” awards in 2016. First employee to do so in a single year.

#### Upwork.com | _Software Developer_ | 2011-2013

- Assisted in the creation of custom, SaaS websites using Flask, html, CSS, Javascript, & PostgreSQL.
- Maintained legacy SOAP APIs while contributing to the creation of RESTful APIs.
- Collaborated with cross-functional teams to troubleshoot & resolve software bugs & defects.

---

### Education
**Arizona State University** 2007-2011<br>
Double Major in Bioarchaeology & Neuropsychology<br>
